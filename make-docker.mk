docker-push-latest:
	docker tag workshop-devops-app:latest 17111993/workshop-devops-app
	docker push 17111993/workshop-devops-app

docker-build-prod:
	docker build -f services/app/Dockerfile.production -t 'workshop-devops-app:latest' services/app/.

docker-nginx-build:
	docker build -f services/nginx/Dockerfile -t 'devops-nginx:latest' services/nginx/.

docker-nginx-push:
	docker tag devops-nginx:latest 17111993/workshop-devops-app
	docker push 17111993/workshop-devops-app