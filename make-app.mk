app:
	docker-compose up

app-bash:
	docker-compose run app bash

app-setup: development-setup-env app-build
	docker-compose run app bin/setup

app-build:
	docker-compose build

development-setup-env:
	ansible-playbook ansible/development.yml -i ansible/development -vv
